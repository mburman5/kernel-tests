# storage/nvdimm/fsdax_dm_stripe

Storage: nvdimm fsdax dm_strie test

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
